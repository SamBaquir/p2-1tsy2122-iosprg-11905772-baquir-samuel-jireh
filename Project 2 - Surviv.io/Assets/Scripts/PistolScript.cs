﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolScript : GunScript
{
    protected override void Start()
    {
        maxAmmo = this.transform.parent.gameObject.GetComponent<ArmScript>().pistolAmmo;
    }

}
