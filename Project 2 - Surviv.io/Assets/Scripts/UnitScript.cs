﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitScript : MonoBehaviour
{
    public int health = 0;
    public int ammo = 0;
    public float movementSpeed;
    //public Weapon primaryWeapon;
    //public Weapon secondaryWeapon;
    public Rigidbody2D rb;
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void Move()
    {


    }


    public virtual void TakeDamage(int damage)
    {
        health -= damage;
        Death();
    }

    public virtual void Death()
    {
        if (health <= 0) Destroy(this.gameObject);
        else return;
    }
}
