﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolAIState : State
{
    public GameObject owner;

    public DetectionState detectionState;
    public IdleAIState idleAIState;
    public LootState lootState;

    public bool withinDetectionRadius;

    int randomAction = 0;

    public void Start()
    {
        randomAction = Random.Range(0, 3);
    }

    public override State RunCurrentState()
    {
        Debug.Log("hatdog");
        owner.gameObject.GetComponent<EnemyUnitScript>().Patrol();

        if (withinDetectionRadius)
        {
            return detectionState;
        }
        else
        {
            if (randomAction < 0.5)
            {
                return idleAIState;
                
            }
            else if (randomAction >= 0.5 && randomAction <= 2)
            {
                return this;
            }
           
            else
            {
                return lootState;
            }

        }
    }
}
