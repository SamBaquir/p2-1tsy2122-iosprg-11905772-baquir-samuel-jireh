﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleAIState : State
{
    public GameObject owner;

    public DetectionState detectionState;
    public PatrolAIState patrolAiState;
    public LootState lootState;

    public bool withinDetectionRadius;

    public int randomAction;

    public void Start()
    {
        randomAction = Random.Range(0, 3);
    }
   
    public override State RunCurrentState()
    {
        if (withinDetectionRadius)
        {
            return detectionState;
        }
        else
        {
            if (randomAction < 1)
            {
                return this;
            }
            else if (randomAction >= 1 && randomAction <= 2)
            {
                return patrolAiState;
            }
            else
            {
                return lootState;
            }
            
        }
        
    }
}
