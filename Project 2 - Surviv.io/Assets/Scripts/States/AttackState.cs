﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : State
{
    public GameObject owner;

    public DetectionState detectionState;

    public bool stillInAttackRadius;

    public override State RunCurrentState()
    {
        if (stillInAttackRadius)
        {
            Debug.Log("Attacking");
            return this;
        }
        else
        {
            detectionState.GetComponent<DetectionState>().isInAttackRange = false;
            return detectionState;

        }
    }
}
