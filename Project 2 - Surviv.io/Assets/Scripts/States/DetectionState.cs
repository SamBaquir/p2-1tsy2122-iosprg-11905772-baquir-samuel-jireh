﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionState : State
{
    public GameObject owner;

    public PatrolAIState patrolAiState;
    public IdleAIState idleAIState;
    public LootState lootState;

    public bool isInAttackRange;
    public int randomAction;

    public void Start()
    {
        randomAction = Random.Range(0, 3);
    }

    public override State RunCurrentState()
    {
        if (isInAttackRange)
        {
            owner.GetComponent<EnemyUnitScript>().Detect();
            Debug.Log("Detecting");
            return this;
        }
        else
        {
            if (randomAction < 1)
            {
                return idleAIState;
            }
            else if (randomAction >= 1 && randomAction <= 2) 
            {
                return patrolAiState;
            }
                       
            else
            {
                return lootState;
            }

            
        }
        
    }
}
