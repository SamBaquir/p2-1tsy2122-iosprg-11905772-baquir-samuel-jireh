﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPickupScript : PickupScript
{
    public string ammoTypeName = "";

    void Start()
    {
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, -1);
    }

    public override void OnTriggerEnter2D(Collider2D col)
    {
        if (ammoTypeName == "9mm" && col.gameObject.tag == "Player")
        {
            col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().doAddPistolAmmo();
            base.OnTriggerEnter2D(col);
        }
        else if (ammoTypeName == "5.56mm" && col.gameObject.tag == "Player")
        {
            col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().doAddRifleAmmo();
            base.OnTriggerEnter2D(col);
        }
        else if (ammoTypeName == "12gauge" && col.gameObject.tag == "Player")
        {
            col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().doAddShotgunAmmo();
            base.OnTriggerEnter2D(col);
        }

    }
}
