﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunScript : GunScript
{
    protected override void Start()
    {
        maxAmmo = this.transform.parent.gameObject.GetComponent<ArmScript>().shotgunAmmo;
    }

    public override void Fire()
    {
        if (currentClipCapacity > 0)
        {
            for (int i = 0; i < 8; i++)
            {
                GameObject bullet = Instantiate(bulletPreFab, GetSpreadArea(), nozzle.transform.rotation);
                bullet.gameObject.GetComponent<BulletScript>().damage = 20;
                Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
                rb.AddForce(nozzle.up * bulletForce, ForceMode2D.Impulse);
            }
            currentClipCapacity--;
        }
        else
        {
            if (maxAmmo > clipCapacity)
            {
                maxAmmo -= clipCapacity;
                currentClipCapacity = clipCapacity;
            }
            else
            {
                currentClipCapacity = maxAmmo;
                maxAmmo = 0;
            }
        }
    }

    Vector2 GetSpreadArea()
    {
        float x = Random.Range(nozzle.transform.position.x + 0.5f, nozzle.transform.position.x - 0.5f);
        float y = Random.Range(nozzle.transform.position.y, nozzle.transform.position.y);

        return new Vector2(x, y);
    }
}
