﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    public GameObject[] objectsToSpawn;
    public SpriteRenderer spriteRenderer;
    public Bounds bounds;

    void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 20; i++)
        {
            Instantiate(objectsToSpawn[UnityEngine.Random.Range(0, objectsToSpawn.Length)], GetSpawnPoint(), Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(spriteRenderer.bounds.extents.x);
    }

    Vector2 GetSpawnPoint()
    {
        float x = Random.Range(-spriteRenderer.bounds.extents.x + 1, spriteRenderer.bounds.extents.x - 1);
        float y = Random.Range(-spriteRenderer.bounds.extents.y + 1, spriteRenderer.bounds.extents.y - 1);

        return new Vector2(x, y);
    }

}
