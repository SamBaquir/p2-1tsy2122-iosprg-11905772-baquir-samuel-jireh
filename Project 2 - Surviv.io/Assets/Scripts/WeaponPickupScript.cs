﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickupScript : PickupScript
{
    public string weaponTypeName = "";
    public GameObject weaponTypeToPickup;

    public override void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Enemy") return;

        if ((col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().currentGun != null &&
            col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().primaryWeapon != null) ||
            (col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().currentGun != null &&
            col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().secondaryWeapon != null)) return;

        if (col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().currentGun == null)
        {
            if (weaponTypeName == "Pistol")
            {
                GameObject weaponSpawn = Instantiate(weaponTypeToPickup, this.transform.position, this.transform.rotation);
                col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().doEquipPistol(weaponSpawn);
            }
            else if (weaponTypeName == "Rifle")
            {
                GameObject weaponSpawn = Instantiate(weaponTypeToPickup, this.transform.position, this.transform.rotation);
                col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().doEquipRifle(weaponSpawn);
            }
            else if (weaponTypeName == "Shotgun")
            {
                GameObject weaponSpawn = Instantiate(weaponTypeToPickup, this.transform.position, this.transform.rotation);
                col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().doEquipShotgun(weaponSpawn);
            }
        }

        else if (col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().currentGun != null)
        {
            if (weaponTypeName == "Pistol")
            {
                if (col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().currentGun.GetComponent<GunScript>().weaponType == weaponTypeName ||
                    col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().secondaryWeapon != null) return;

                GameObject weaponSpawn = Instantiate(weaponTypeToPickup, this.transform.position, this.transform.rotation);
                col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().secondaryWeapon = weaponSpawn;
                weaponSpawn.GetComponent<GunScript>().maxAmmo = col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().pistolAmmo;
                weaponSpawn.transform.parent = col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().transform;
                weaponSpawn.transform.localPosition = new Vector3(0, 1f, 0);
                weaponSpawn.transform.localRotation = Quaternion.Euler(0, col.transform.GetChild(0).gameObject.transform.rotation.y, 0);
                weaponSpawn.SetActive(false);
            }

            else if (weaponTypeName == "Rifle")
            {
                if (col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().currentGun.GetComponent<GunScript>().weaponType == weaponTypeName || 
                    col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().currentGun.GetComponent<GunScript>().weaponType == "Shotgun") return;

                GameObject weaponSpawn = Instantiate(weaponTypeToPickup, this.transform.position, this.transform.rotation);
                col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().primaryWeapon = weaponSpawn;
                weaponSpawn.GetComponent<GunScript>().maxAmmo = col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().rifleAmmo;
                weaponSpawn.transform.parent = col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().transform;
                weaponSpawn.transform.localPosition = new Vector3(0, 1f, 0);
                weaponSpawn.transform.localRotation = Quaternion.Euler(0, col.transform.GetChild(0).gameObject.transform.rotation.y, 0);
                weaponSpawn.SetActive(false);
            }

            else if (weaponTypeName == "Shotgun")
            {
                if (col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().currentGun.GetComponent<GunScript>().weaponType == weaponTypeName ||
                    col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().currentGun.GetComponent<GunScript>().weaponType == "Rifle") return;

                GameObject weaponSpawn = Instantiate(weaponTypeToPickup, this.transform.position, this.transform.rotation);
                col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().primaryWeapon = weaponSpawn;
                weaponSpawn.GetComponent<GunScript>().maxAmmo = col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().shotgunAmmo;
                weaponSpawn.transform.parent = col.transform.GetChild(0).gameObject.GetComponent<ArmScript>().transform;
                weaponSpawn.transform.localPosition = new Vector3(0, 1f, 0);
                weaponSpawn.transform.localRotation = Quaternion.Euler(0, col.transform.GetChild(0).gameObject.transform.rotation.y, 0);
                weaponSpawn.SetActive(false);
            }
        }
        base.OnTriggerEnter2D(col);
    }
}
