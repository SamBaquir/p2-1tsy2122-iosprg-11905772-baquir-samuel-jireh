﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemyToSpawn;
    public SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {

        for (int i = 0; i < 10; i++)
        {
            Instantiate(enemyToSpawn, GetSpawnPoint(), Quaternion.identity);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    Vector2 GetSpawnPoint()
    {
        float x = Random.Range(-spriteRenderer.bounds.extents.x + 1, spriteRenderer.bounds.extents.x - 1);
        float y = Random.Range(-spriteRenderer.bounds.extents.y + 1, spriteRenderer.bounds.extents.y - 1);

        return new Vector2(x, y);
    }
}
