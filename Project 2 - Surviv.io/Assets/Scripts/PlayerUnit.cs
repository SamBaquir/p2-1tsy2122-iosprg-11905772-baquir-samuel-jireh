﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUnit : UnitScript
{
    public GameObject gameManager;
    public int currentHealth;
    public HealthBar healthBar;
  
    float horizontalMove = 0f, verticalMove = 0f;

    public Joystick moveJoystick, lookJoystick;

    public SpriteRenderer spriteRenderer;

    public GameObject arm;
    //public GameObject firstGun;

    void Awake()
    {
        currentHealth = health;
        healthBar.SetMaxHealth(health);
        rb = this.GetComponent<Rigidbody2D>();
        arm = this.transform.GetChild(0).gameObject;
        //firstGun = Instantiate(firstGun, arm.transform.position, arm.transform.rotation);
        //firstGun.transform.parent = arm.transform;
    }

    // Start is called before the first frame update
    void Start()
    {
        //firstGun.transform.position = new Vector2(arm.transform.position.x, arm.transform.position.y + 0.5f);
    }
    
    // Update is called once per frame
    void Update()
    {
        //Input float
        horizontalMove = moveJoystick.Horizontal;
        verticalMove = moveJoystick.Vertical;

        //Move Joystick
        Vector3 moveTempVect = new Vector3(horizontalMove, verticalMove, 0);
        moveTempVect = moveTempVect.normalized * movementSpeed * Time.deltaTime;
        rb.MovePosition(transform.position + moveTempVect);

        //Boundary of the map
        if (transform.position.y >= 24 || transform.position.x >= 44)
        {
            transform.position = new Vector3(transform.position.x - 0.01f, transform.position.y - 0.01f, 0);
        }
        if (transform.position.y <= -24 || transform.position.x <= -44)
        {
            transform.position = new Vector3(transform.position.x + 0.01f, transform.position.y + 0.01f, 0);
        }

        //Look Joystick
        Vector3 moveVector = (Vector3.up * lookJoystick.Horizontal + Vector3.left * lookJoystick.Vertical);
        if (lookJoystick.Horizontal != 0 || lookJoystick.Vertical != 0)
        {
            transform.rotation = Quaternion.LookRotation(Vector3.forward, moveVector);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            TakeDamage(20);
        }
    }

    public override void TakeDamage(int damage)
    {
        currentHealth -= damage;

        healthBar.SetHealth(currentHealth);
        Death();
    }

    public override void Death()
    {
        if (currentHealth <= 0)
        {
            gameManager.GetComponent<GameManager>().RestartLevel();
            this.gameObject.SetActive(false);

        }
        else return;
    }
}