﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GunScript : MonoBehaviour
{
    public GameObject bulletPreFab;
    public Transform nozzle;

    public float bulletForce = 20f;

    public int maxAmmo = 100, currentClipCapacity = 0, clipCapacity = 0;
    public float fireRate = 0;
    public string weaponType = "";

    protected virtual void Awake()
    {
        currentClipCapacity = clipCapacity;
    }

    // Start is called before the first frame update
    protected virtual void Start()
    {
    }

    // Update is called once per frame
    protected virtual void Update()
    {
    }

    public virtual void Fire()
    {
        if (currentClipCapacity > 0)
        {
            GameObject bullet = Instantiate(bulletPreFab, nozzle.transform.position, nozzle.transform.rotation);
            if (weaponType == "Pistol")
            {
                bullet.gameObject.GetComponent<BulletScript>().damage = 10;
            }
            else if (weaponType == "Rifle")
            {
                bullet.gameObject.GetComponent<BulletScript>().damage = 15;
            }
            else
            {
                bullet.gameObject.GetComponent<BulletScript>().damage = 20;
            }
            Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
            rb.AddForce(nozzle.up * bulletForce, ForceMode2D.Impulse);
            currentClipCapacity--;
            Debug.Log(currentClipCapacity);
        }
        else
        {
            if (maxAmmo > clipCapacity)
            {
                maxAmmo -= clipCapacity;
                currentClipCapacity = clipCapacity;
            }
            else
            {
                currentClipCapacity = maxAmmo;
                maxAmmo = 0;
            }
        }
    }
}
