﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ArmScript : MonoBehaviour
{
    public TextMeshProUGUI currentClipUI, maxAmmoUI, pistolAmmoUI, rifleAmmoUI, shotgunAmmoUI;
    public GameObject currentGun, primaryWeapon, secondaryWeapon;
    public int currentGunCurrentAmmo, currentGunMaxAmmo;
    public int pistolAmmo, rifleAmmo, shotgunAmmo;
    bool isFiring, stopFiring;

    private IEnumerator coroutine;

    void Awake()
    {
        /*
        if (currentGun != null)
        {
            currentGun = this.transform.GetChild(0).gameObject;
        }
        */
    }

    // Start is called before the first frame update
    void Start()
    {
        if (currentGun != null)
        {
            currentClipUI.text = currentGunCurrentAmmo.ToString();
            maxAmmoUI.text = currentGunMaxAmmo.ToString();
        }
        pistolAmmoUI.text = pistolAmmo.ToString();
        rifleAmmoUI.text = rifleAmmo.ToString();
        shotgunAmmoUI.text = shotgunAmmo.ToString();
    }

    public void pointerDown()
    {
        stopFiring = false;
        Invoke("makeFireVariableTrue", 0.2f);
    }

    public void pointerUp()
    {
        isFiring = false;
        stopFiring = true;
    }

    void makeFireVariableTrue()
    {
        isFiring = true;
    }

    void makeFireVariableFalse()
    {
        isFiring = false;
        if (stopFiring == false)
        {
            Invoke("makeFireVariableTrue", 0.2f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isFiring && currentGun != null)
        {
            makeFireVariableFalse();
            if (currentGun.GetComponent<GunScript>().weaponType == "Rifle")
            {
                if (this.transform.GetChild(0).gameObject == null)
                {
                    return;
                }
                currentGun.GetComponent<GunScript>().Fire();
            }
        }

        if (currentGun != null)
        {
            currentGunMaxAmmo = currentGun.GetComponent<GunScript>().maxAmmo;
            currentGunCurrentAmmo = currentGun.GetComponent<GunScript>().currentClipCapacity;
            currentClipUI.text = currentGunCurrentAmmo.ToString();
            maxAmmoUI.text = currentGunMaxAmmo.ToString();
        }
        else
        {
            currentGunMaxAmmo = 0;
            currentGunCurrentAmmo = 0;
            currentClipUI.text = currentGunCurrentAmmo.ToString();
            maxAmmoUI.text = currentGunMaxAmmo.ToString();
        }

        pistolAmmoUI.text = pistolAmmo.ToString();
        rifleAmmoUI.text = rifleAmmo.ToString();
        shotgunAmmoUI.text = shotgunAmmo.ToString();

        if (currentGun != null)
        {
            if (currentGun.GetComponent<GunScript>().weaponType == "Pistol")
            {
                pistolAmmo = currentGun.GetComponent<GunScript>().maxAmmo;
                pistolAmmoUI.text = pistolAmmo.ToString();
                rifleAmmoUI.text = rifleAmmo.ToString();
                shotgunAmmoUI.text = shotgunAmmo.ToString();
            }

            else if (currentGun.GetComponent<GunScript>().weaponType == "Rifle")
            {
                rifleAmmo = currentGun.GetComponent<GunScript>().maxAmmo;
                pistolAmmoUI.text = pistolAmmo.ToString();
                rifleAmmoUI.text = rifleAmmo.ToString();
                shotgunAmmoUI.text = shotgunAmmo.ToString();
            }

            else if (currentGun.GetComponent<GunScript>().weaponType == "Shotgun")
            {
                shotgunAmmo = currentGun.GetComponent<GunScript>().maxAmmo;
                pistolAmmoUI.text = pistolAmmo.ToString();
                rifleAmmoUI.text = rifleAmmo.ToString();
                shotgunAmmoUI.text = shotgunAmmo.ToString();
            }
        }
    }

    public void UseGun()
    {
        if (currentGun.GetComponent<GunScript>().weaponType != "Rifle")
        {
            if (this.transform.GetChild(0).gameObject == null)
            {
                return;
            }
            currentGun.GetComponent<GunScript>().Fire();
        }
    }



    public void equipPrimaryWeapon()
    {
        if (primaryWeapon == null) return;

        if (currentGun != null && currentGun.GetComponent<GunScript>().weaponType == "Pistol")
        {
            secondaryWeapon = currentGun;
            currentGun = null;
            primaryWeapon.SetActive(true);
            currentGun = primaryWeapon;
            primaryWeapon = null;
            secondaryWeapon.SetActive(false);
            /*
            primaryWeapon.SetActive(true);
            secondaryWeapon = currentGun;
            secondaryWeapon.SetActive(false);
            currentGun = null;
            currentGun = primaryWeapon;
            */
        }
        if (rifleAmmo <= 0 || shotgunAmmo <= 0)
        {
            rifleAmmo = 0;
            shotgunAmmo = 0;
        }
        else
        {
            if (currentGun.GetComponent<GunScript>().weaponType == "Rifle")
            {
                currentGun.GetComponent<GunScript>().maxAmmo = rifleAmmo;
            }
            else if (currentGun.GetComponent<GunScript>().weaponType == "Shotgun")
            {
                currentGun.GetComponent<GunScript>().maxAmmo = shotgunAmmo;
            }
        }
    }

    public void equipSecondaryWeapon()
    {
        if (secondaryWeapon == null) return;

        if (currentGun != null && (currentGun.GetComponent<GunScript>().weaponType == "Rifle" || 
            currentGun.GetComponent<GunScript>().weaponType == "Shotgun"))
        {
            primaryWeapon = currentGun;
            currentGun = null;
            secondaryWeapon.SetActive(true);
            currentGun = secondaryWeapon;
            secondaryWeapon = null;
            primaryWeapon.SetActive(false);
            /*
            primaryWeapon = currentGun;
            currentGun = null;
            currentGun = primaryWeapon;
            */
        }

        if (pistolAmmo <= 0)
        {
            pistolAmmo = 0;
        }
        else
        {
            currentGun.GetComponent<GunScript>().maxAmmo = pistolAmmo;
        }
    }

    public void doEquipPistol(GameObject weaponObject)
    {
        equipPistol(weaponObject);
    }

    public void doEquipRifle(GameObject weaponObject)
    {
        equipRifle(weaponObject);
    }

    public void doEquipShotgun(GameObject weaponObject)
    {
        equipShotgun(weaponObject);
    }

    void equipPistol(GameObject weaponObjectToEquip)
    {
        weaponObjectToEquip.transform.parent = this.transform;
        weaponObjectToEquip.transform.localRotation = Quaternion.Euler(0, this.transform.rotation.y, 0);
        weaponObjectToEquip.transform.localPosition = new Vector3(0, 1f, 0);
        weaponObjectToEquip.GetComponent<GunScript>().maxAmmo = pistolAmmo;
        currentGun = weaponObjectToEquip;
        /*
        if (secondaryWeapon == null)
        {
            secondaryWeapon = currentGun;
        }
        */
    }

    void equipRifle(GameObject weaponObjectToEquip)
    {
        weaponObjectToEquip.transform.parent = this.transform;
        weaponObjectToEquip.transform.localRotation = Quaternion.Euler(0, this.transform.rotation.y, 0);
        weaponObjectToEquip.transform.localPosition = new Vector3(0, 1f, 0);
        weaponObjectToEquip.GetComponent<GunScript>().maxAmmo = rifleAmmo;
        currentGun = weaponObjectToEquip;
        /*
        if (primaryWeapon == null)
        {
            primaryWeapon = currentGun;
        }
        */
        
    }

    void equipShotgun(GameObject weaponObjectToEquip)
    {
        weaponObjectToEquip.transform.parent = this.transform;
        weaponObjectToEquip.transform.localRotation = Quaternion.Euler(0, this.transform.rotation.y, 0);
        weaponObjectToEquip.transform.localPosition = new Vector3(0, 1f, 0);
        weaponObjectToEquip.GetComponent<GunScript>().maxAmmo = shotgunAmmo;
        currentGun = weaponObjectToEquip;
        /*
        if (primaryWeapon == null)
        {
            primaryWeapon = currentGun;
        }
        */
    }

    public void doAddPistolAmmo()
    {
        addPistolAmmo();
    }

    public void doAddRifleAmmo()
    {
        addRifleAmmo();
    }

    public void doAddShotgunAmmo()
    {
        addShotgunAmmo();
    }

    void addPistolAmmo()
    {
        pistolAmmo += 20;
        if (currentGun == null) return;
        if (currentGun.GetComponent<GunScript>().weaponType == "Pistol")
        {
            pistolAmmo += 20;
            currentGunMaxAmmo += 20;
            currentGun.GetComponent<GunScript>().maxAmmo += 20;
        }
    }
    void addRifleAmmo()
    {
        rifleAmmo += 20;
        if (currentGun == null) return;
        if (currentGun.GetComponent<GunScript>().weaponType == "Rifle")
        {
            rifleAmmo += 20;
            currentGunMaxAmmo += 20;
            currentGun.GetComponent<GunScript>().maxAmmo += 20;
        }
    }
    void addShotgunAmmo()
    {
        shotgunAmmo += 20;
        if (currentGun == null) return;
        if (currentGun.GetComponent<GunScript>().weaponType == "Shotgun")
        {
            shotgunAmmo += 20;
            currentGunMaxAmmo += 20;
            currentGun.GetComponent<GunScript>().maxAmmo += 20;
        }
    }
}
