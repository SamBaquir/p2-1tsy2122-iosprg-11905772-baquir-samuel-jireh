﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public int damage = 0;

    void Awake()
    {


    }

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        Destroy(this.gameObject, 2f);
    }

    public virtual void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            Physics2D.IgnoreCollision(col.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }
        else
        {
            if (Vector2.Distance(this.transform.position, col.transform.position) < 0.5f)
            {
                if (col.gameObject.tag == "Player")
                {
                    col.gameObject.GetComponent<PlayerUnit>().TakeDamage(damage);
                }
                else if (col.gameObject.tag =="Enemy")
                {
                    col.gameObject.GetComponent<UnitScript>().TakeDamage(damage);
                }

                Debug.Log("destroy me");
                Destroy(gameObject);
            }

        }
        /*
        if (col.gameObject.tag != "Bullet")
        {
            Debug.Log("destroy me");
            Destroy(gameObject);
        }
        */
    }

    public virtual void OnCollisionEnter2D(Collision2D collision)
    {
        /*
        if (collision.gameObject.tag == "Bullet")
        {
            Physics2D.IgnoreCollision(collision.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }
        else
        {
            Debug.Log("destroy me");
            Destroy(gameObject);
        }
        */

    }
}
