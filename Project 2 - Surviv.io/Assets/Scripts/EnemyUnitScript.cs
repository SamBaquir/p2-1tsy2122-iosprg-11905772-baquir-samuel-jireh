﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyUnitScript : UnitScript
{
    private float waitTime;
    public float startWaitTime;

    public GameObject[] gunsToEquip;
    public int pistolAmmo, rifleAmmo, shotgunAmmo;
    public GameObject target, MoveSpot, currentGun;
    public Transform moveSpot;
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;

    public float timer = 0;
    public float currentTimer = 0;

    public float reloadTimer = 300;
    public float currentReloadTimer = 0;

    public State[] states;

    void Awake()
    {
        rb = this.GetComponent<Rigidbody2D>();
        MoveSpot = GameObject.Find("MoveSpot");
        moveSpot = MoveSpot.transform;

        currentGun = Instantiate(gunsToEquip[UnityEngine.Random.Range(0, gunsToEquip.Length)], transform);
        currentGun.transform.localRotation = Quaternion.Euler(0, this.transform.rotation.y, 0);
        currentGun.transform.localPosition = new Vector3(0.2f, 0.9f, 0);
    }

    void Start()
    {
        waitTime = startWaitTime;

        moveSpot.position = new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY));
    }

    public void Patrol()
    {
        rb.position = Vector2.MoveTowards(transform.position, moveSpot.position, movementSpeed * Time.deltaTime);
        transform.up = moveSpot.transform.position - transform.position;

        if (Vector2.Distance(transform.position, moveSpot.position) < 1f)
        {
            if (waitTime <= 0)
            {
                moveSpot.position = new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY));
                waitTime = startWaitTime;
            }
            else
            {
                waitTime -= Time.deltaTime;
            }
        }

    }

    public void Detect()
    {
        timer = currentGun.GetComponent<GunScript>().fireRate;
        transform.up = target.transform.position - transform.position;

        if (currentTimer < timer)
        {
            currentTimer++;
        }
        else
        {
            if (currentGun.GetComponent<GunScript>().currentClipCapacity <= 0)
            {
                currentReloadTimer++;
                if (currentReloadTimer >= reloadTimer)
                {
                    currentReloadTimer = 0;
                    fireCurrentGun();
                    return;
                }

            }
            else
            {
                
                fireCurrentGun();
                currentTimer = 0;
                
            };
        }
    }

    void fireCurrentGun()
    {
        if (currentGun == null) return;
        currentGun.GetComponent<GunScript>().Fire();
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.GetComponent<UnitScript>() && Vector2.Distance(this.transform.position, col.transform.position) < 5f)
        {
            states[0].GetComponent<IdleAIState>().withinDetectionRadius = true;
            states[1].GetComponent<DetectionState>().isInAttackRange = true;
            states[2].GetComponent<PatrolAIState>().withinDetectionRadius = true;
            target = col.gameObject;
            //Debug.Log(states[1]);
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {

        if (col.gameObject == target)
        {
            states[0].GetComponent<IdleAIState>().withinDetectionRadius = false;
            states[1].GetComponent<DetectionState>().isInAttackRange = false;
            states[2].GetComponent<PatrolAIState>().withinDetectionRadius = false;
            target = null;
            //Debug.Log(states[1]);
        }
    }




}
